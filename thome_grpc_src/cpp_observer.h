#include <iostream>
#include <vector>
#include "Requestheader.h"
#include "Responseheader.h"

using namespace std;

class Subject
{
	vector < class ReqObserver * > requests;
	vector < class ResObserver * > responses;
	public:
		void Reqattach(ReqObserver *obs);
		void Resattach(ResObserver *obs);
		void setRequest (Request_class *);
		void Reqnotify();
		void setResponse (t_things *);
		void Resnotify();
};

class ReqObserver
{
	Request_class *req_obj;
	public:
		ReqObserver(Request_class *r)
		{
			req_obj = r;
		}
};

class ResObserver
{
	t_things *t;
	public:
		ResObserver(t_things *tt)
		{
			t = tt;
		}
};

Subject s;
void Subject :: Reqattach (ReqObserver *obs)
{
	 requests.push_back(obs);
}
void Subject :: Resattach (ResObserver *obs)
{
	responses.push_back(obs);
}
//client side 

		
