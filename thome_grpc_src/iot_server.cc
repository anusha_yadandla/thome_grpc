#include <iostream>
#include <memory>
#include <string>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include<cstring>
#include <unistd.h>
#include<stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <grpc++/grpc++.h>
#include <pthread.h>
#include <thread>
#include<future>
#include<vector>
#ifdef BAZEL_BUILD
#include "examples/protos/iot.grpc.pb.h"
#else
#include "iot.grpc.pb.h"
#endif

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;
using grpc::Status;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerWriter;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::string_ref;
using iot :: RequestObject;
using iot :: ResponseObject;
using iot :: NondiscoveryObject;
using iot :: status;
using iot::Sender;
using iot::tthing;
using iot::device;
using iot::tproperty;
using namespace std;

class ResObject
{
	 vector <class Tthing *> Tthings;
	 public:
	 void add_tthing(Tthing *tth)
	 {
	 	Tthings.push_back(tth);
	 }
};

class Tthing
{
	string thingsName;
	string osName ;
	string thingsType ;
	string hardwareVersion ;
	string manufacturerName;
	string uuid;
	vector <class Device *> Devices;
	public :
	void add_device(Device *d)
	{
		Devices.push_back(d);
	}
	void set_thingsName (string tn)
	{
		thingsName = tn;
	}
	void set_osName (string on)
	{
		osName = on;
	}
	void set_thingsType (string tt)
	{
		thingsType = tt;
	}
	void set_hardwareVersion(string hv)
	{
		hardwareVersion = hv;
	}
	void set_manufacturerName(string mn)
	{
		manufacturerName = mn;
	}
	void set_uuid (string id)
	{
		uuid = id;
	}
	
};

class Device
{
	string dId;
	vector<class Property *> Properties;
	public:
	void add_property(Property *p)
	{
		Properties.push_back(p);
	}
	void set_dId(string id)
	{
		dId = id;
	}
};

class Property
{
	string property_name;
	string property_type;
	string property_value;
	string property_map;
	public :
	void set_pname (string pn)
	{
		property_name = pn;
	}
	void set_ptype (string pt)
	{
		property_type = pt;
	}
	void set_pvalue (string pv)
	{
		property_value = pv;
	}
	void set_pmap (string pm)
	{
		property_map = pm;
	}
};


class client
{
	public:
	client(shared_ptr<Channel> channel):stub_(Sender::NewStub(channel)) {}
	bool DiscoveryResponse(const RequestObject* r)
	{
		/*Property p1,p2;
		Device dev1;
		Tthing tth1;
		ResObject r;
		p1.set_pname("value");
		p1.set_ptype("boolean");
		p1.set_pvalue("0");
		p1.set_pmap("binaryswitch_2");
		
		p2.set_pname("brightness");
		p2.set_ptype("int");
		p2.set_pvalue("50");
		p2.set_pmap("brightness_2");
		
		dev1.set_dId("SMLD1M_1");
		dev1.add_property(&p1);
		dev1.add_property(&p2);
		
		tth1.add_device(&dev1);
		tth1.set_thingsName("Modular_Fan_with_LED");
		tth1.set_osName("linux");
		tth1.set_thingsType("tronFAL01M");
		tth1.set_hardwareVersion("1.1.1");
		tth1.set_manufacturerName("Smartron");
		tth1.set_uuid("93000000-9d00-4000-6a00-000095000000");
		
		r.add_tthing(&tth1);*/
		
		ResponseObject res;
		tthing *t = res.add_tthings();
		tthing *t1 = res.add_tthings();
		device *d = t->add_devices();
		device *d1 = t->add_devices();
		d = t1->add_devices();
		d1 = t1->add_devices();
		tproperty *p11 = d->add_properties();
		tproperty *p12 = d ->add_properties();
		tproperty *p13 = d ->add_properties();
		tproperty *p21 = d1->add_properties();
		p11->set_property_name("value");
		p11->set_property_type("boolean");
		p11->set_property_value("0");
		p11->set_property_map("binaryswitch_2");
		
		p12->set_property_name("brightness");
		p12->set_property_type("int");
		p12->set_property_value("50");
		p12->set_property_map("brightness_2");
		d->set_did("SMLD1M_1");
		d1->set_did("SMFN1M_1");
		
		p13->set_property_name("dimmingSetting");
		p13->set_property_type("int");
		p13->set_property_value("50");
		p13->set_property_map("dimming_2");
		
		p21->set_property_name("value");
		p21->set_property_type("boolean");
		p21->set_property_value("0");
		p21->set_property_map("binaryswitch_1");
		
		
		t->set_thingsname("Modular_Fan_with_LED");
		t->set_osname("linux");
		t->set_thingstype("tronFAL01M");
		t->set_hardwareversion("1.1.1");
		t->set_manufacturername("Smartron");
		t->set_uuid("93000000-9d00-4000-6a00-000095000000");

		t1->set_thingsname("Modular_Fan_with_LED");
		t1->set_osname("linux");
		t1->set_thingstype("tronFAL01M");
		t1->set_hardwareversion("1.1.1");
		t1->set_manufacturername("Smartron");
		t1->set_uuid("93000000-9d00-4000-6a00-000095000000");
		
		status reply; 
		ClientContext context;
		Status stat = stub_->DiscoveryResponse(&context,res,&reply);
		if(stat.ok())
		{
			return reply.message();
		}
		else
		{
			cout<<stat.error_code()<<":"<<stat.error_message()<<endl;
			return "RPC FAILED";
		}
	}
	bool NondiscoveryResponse(const RequestObject* r)
	{
		string command = r->command();
		string uuid = r->uuid();
		string devid = r->devid();
		NondiscoveryObject go;
		device *d = go.mutable_dev();
		tproperty *p11 = d->add_properties();
		if(r->command() == "getting")
			d->set_did("get_SMLD1M_1");
		else
			d->set_did("post_SMLD1M_1");
		p11->set_property_name("value");
		p11->set_property_type("boolean");
		p11->set_property_value("0");
		p11->set_property_map("binaryswitch_2");
		go.set_uuid("93000000-9d00-4000-6a00-000095000000");
		
		status reply; 
		ClientContext context;
		Status stat = stub_->NondiscoveryResponse(&context,go,&reply);
		if(stat.ok())
		{
			return reply.message();
		}
		else
		{
			cout<<stat.error_code()<<":"<<stat.error_message()<<endl;
			return false;
		}
		
	}
	
	private:
	unique_ptr<Sender::Stub> stub_;
};
int flag = 1;

void response_method(const RequestObject* ro,string_ref d)
{
	bool stats;
	// if metadata is used 
	int size = d.size();
	string s = d.data();
	string dest_add = s.substr(0,size);
	
	string st = ro->command();
	if(st == "discovering") 
	{
		//client c(grpc::CreateChannel(dest_add,grpc::InsecureChannelCredentials()));
		client c(grpc::CreateChannel("localhost:1111",grpc::InsecureChannelCredentials()));
		stats = c.DiscoveryResponse(ro);
			
	}	
	else 
	{
		//client c(grpc::CreateChannel(dest_add,grpc::InsecureChannelCredentials()));
		client c(grpc::CreateChannel("localhost:1111",grpc::InsecureChannelCredentials()));
		stats = c.NondiscoveryResponse(ro);
	}
	cout << stats <<endl;
}

class serviceImpl final : public Sender::Service 
{
	Status Request(ServerContext* context, const RequestObject* ro,
                    status* stats) override 
    {
    	// if metadata is used 
    	std::multimap<grpc::string_ref, grpc::string_ref> metadata = context->client_metadata();
    	auto id = metadata.find("client_id");
    	auto src = metadata.find("src_add");
    	auto dest = metadata.find("dest_add");
    	
    	if(ro->command() == "getting"|| ro->command() == "posting" || ro->command() == "discovering")
    	{   	
    		stats->set_message(true);
    		thread(response_method,ro,dest->second).detach();
    	}
    	else
    		stats->set_message(false);
    	return Status::OK;		   	
    }
};

void runserver()
{
	string server_add("0.0.0.0:2222");
	serviceImpl service;
	ServerBuilder builder;
  // Listen on the given address without any authentication mechanism.
  builder.AddListeningPort(server_add, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which we'll communicate with
  // clients. In this case it corresponds to an *synchronous* service.
  builder.RegisterService(&service);
  // Finally assemble the server.
  unique_ptr<Server> server(builder.BuildAndStart());
  cout << "Server listening on " << server_add <<endl;
  // Wait for the server to shutdown. Note that some other thread must be
  // responsible for shutting down the server for this call to ever return.
  server->Wait();
}

int main(int argc, char** argv) 
{

  runserver();
  return 0;
}
