var grpc = require('grpc');
var async = require('async');
var messages = require('./iot_pb');
var services = require('./iot_grpc_pb');
var ip = require('ip');
var Sync = require('sync');
var getmac = require('getmac');
var stdin = process.openStdin();
var cl = new services.SenderClient('localhost:1111', grpc.credentials.createInsecure());

function trial(call,callback)
{
	var req = new messages.ResponseObject();
	var p = new messages.property();
	p.setPropertyName("value");
	p.setPropertyType("boolean");
	p.setPropertyValue("0");
	p.setPropertyMap("binaryswitch_2");
	
	var d = new messages.device();
	d.addProperties(p);
	//console.log(d.getPropertiesList());
	d.setDid("SMLD1M_1");
	
	var t = new messages.tthing();
	t.setThingsname("Modular_Fan_with_LED");
	t.setOsname("linux");
	t.setThingstype("tronFAL01M");
	t.setHardwareversion("1.1.1");
	t.setManufacturername("Smartron");
	t.setUuid("93000000-9d00-4000-6a00-000095000000");
	t.addDevices(d);
	
	var t1 = new messages.tthing();
	t1.setThingsname("Modular_Fan_with_LED");
	t1.setOsname("linux");
	t1.setThingstype("tronFAL01M");
	t1.setHardwareversion("1.1.1");
	t1.setManufacturername("Smartron");
	t1.setUuid("93000000-9d00-4000-6a00-000095000000");
	t1.addDevices(d);
	//t.setDevicesList(d);
	req.addTthings(t);
	
	callback(null,req);
}
function main()
{
	var server = new grpc.Server();
	//server.addService(services.SenderService,{lotsOfReplies : lotsOfReplies,send_details : send_details,lotsOfRequests : lotsOfRequests,bistreaming : bistreaming ,chat : chat});
	server.bind('0.0.0.0:2222',grpc.ServerCredentials.createInsecure());
	
	var req = new messages.ResponseObject();
	var p = new messages.property();
	p.setPropertyName("value");
	p.setPropertyType("boolean");
	p.setPropertyValue("0");
	p.setPropertyMap("binaryswitch_2");
	
	var d = new messages.device();
	d.addProperties(p);
	//console.log(d.getPropertiesList());
	d.setDid("SMLD1M_1");
	
	var d1 = new messages.device();
	d1.addProperties(p);
	//console.log(d.getPropertiesList());
	d1.setDid("SMLD1M_2");
	
	var t = new messages.tthing();
	t.setThingsname("Modular_Fan_with_LED");
	t.setOsname("linux");
	t.setThingstype("tronFAL01M");
	t.setHardwareversion("1.1.1");
	t.setManufacturername("Smartron");
	t.setUuid("93000000-9d00-4000-6a00-000095000000");
	t.addDevices(d);
	t.addDevices(d1);
	
	var t1 = new messages.tthing();
	t1.setThingsname("Modular_Fan_with_LED");
	t1.setOsname("linux");
	t1.setThingstype("tronFAL01M");
	t1.setHardwareversion("1.1.1");
	t1.setManufacturername("Smartron");
	t1.setUuid("93000000-9d00-4000-6a00-000095000000");
	t1.addDevices(d);
	//t.setDevicesList(d);
	req.addTthings(t);
	req.addTthings(t1);
	cl.response(req,function(err,res) {
		console.log(res.getMessage());
	});
	server.start();
}
main();
