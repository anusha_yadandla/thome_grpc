# INSTALLING GRPC 

For language-specific installation instructions for gRPC runtime, please
refer to these documents

 * [C++](examples/cpp): Currently to install gRPC for C++, you need to build from source as described below.
 * [C#](src/csharp): NuGet package `Grpc`
 * [Go](https://github.com/grpc/grpc-go): `go get google.golang.org/grpc`
 * [Java](https://github.com/grpc/grpc-java)
 * [Node](src/node): `npm install grpc`
 * [Objective-C](src/objective-c)
 * [PHP](src/php): `pecl install grpc`
 * [Python](src/python/grpcio): `pip install grpcio`
 * [Ruby](src/ruby): `gem install grpc`

# Pre-requisites - LINUX 
$ [sudo] apt-get install build-essential autoconf libtool

If you plan to build from source and run tests, install the following as well:

 $ [sudo] apt-get install libgflags-dev libgtest-dev
 $ [sudo] apt-get install clang libc++-dev
 
## Protoc 
By default gRPC uses [protocol buffers](https://github.com/google/protobuf),you will need the `protoc` compiler to generate stub server and client code.
### Installing protocol buffers - LINUX 
* Download the protobuf 3.0.0 source code using the following link:
  (https://github.com/google/protobuf/releases/tag/v3.0.0)
* To build protobuf from source,the following tools are needed :
  -> autoconf
  -> automake 
  -> libtool
  ->curl
  ->make
  ->g++
  ->unzip 
  These can be installed using the following command :
	$ [sudo] apt-get install autoconf automake libtool curl make g++ unzip
* Generate the configure script :
	$ ./autogen.sh
* To install the protobuf complier (protoc) 
   
	$ ./configure
	$ make
	$ make check
	$ sudo make install
	$ sudo ldconfig # refresh shared library cache.

	
# GRPC - BUILD FROM SOURCE (LINUX)

For developers who are interested to contribute, here is how to compile the
gRPC C Core library.

```sh
 $ git clone -b $(curl -L http://grpc.io/release) https://github.com/grpc/grpc
 $ cd grpc
 $ git submodule update --init
 $ make
 $ [sudo] make install
```

# Installing Node gRPC protoc plugin 
	npm install -g grpc-tools
	

  
  

